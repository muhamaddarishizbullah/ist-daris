package ist.challenge.daris.service.impl;

import ist.challenge.daris.models.payload.response.UserResponse;
import ist.challenge.daris.repository.UserRepository;
import ist.challenge.daris.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServicesImpl implements UserServices {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public List<UserResponse> getAllUser() {
        List<UserResponse> userResponses = new ArrayList<>();
        userRepository.findAll().forEach(data->{
            UserResponse userResponse = new UserResponse();
            userResponse.setId(data.getId());
            userResponse.setUsername(data.getUsername());
            userResponse.setPassword(data.getPassword());
            userResponses.add(userResponse);
        });
        return userResponses;
    }
}
