package ist.challenge.daris.service;

import ist.challenge.daris.models.payload.response.UserResponse;

import java.util.List;

public interface UserServices {
    List<UserResponse> getAllUser();
}
