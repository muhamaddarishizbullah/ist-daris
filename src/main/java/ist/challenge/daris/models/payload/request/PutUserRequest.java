package ist.challenge.daris.models.payload.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor
@Data
public class PutUserRequest {
    @NotBlank
    @Size(min = 3, max = 25)
    @ApiModelProperty(position = 0)
    private String username;

    @NotBlank
    @ApiModelProperty(position = 1)
    @Size(min = 6, max = 25)
    private String password;
}
