package ist.challenge.daris.models.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class PostJurnalSequenceDataResponse {

    @JsonProperty("PAYMENT_TYPE")
    @ApiModelProperty(position = 0)
    private String paymentType;

    @JsonProperty("JURNAL_SEQUENCE")
    @ApiModelProperty(position = 1)
    private long jurnalSequence;

    @JsonProperty("SEQUENCE_ID")
    @ApiModelProperty(position = 2)
    private long sequenceId;

    @JsonProperty("UPDATED_AT")
    @ApiModelProperty(position = 3)
    private String updatedAt;

    @JsonProperty("CREATED_AT")
    @ApiModelProperty(position = 4)
    private String createdAt;

    @JsonProperty("CREATED_BY")
    @ApiModelProperty(position = 5)
    private String createdBy;

    @JsonProperty("UPDATED_BY")
    @ApiModelProperty(position = 6)
    private String updatedBy;

}
