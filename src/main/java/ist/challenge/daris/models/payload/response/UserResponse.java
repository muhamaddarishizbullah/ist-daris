package ist.challenge.daris.models.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserResponse {

    @JsonProperty("ID")
    @ApiModelProperty(position = 0)
    private Long id;

    @JsonProperty("USERNAME")
    @ApiModelProperty(position = 1)
    private String username;

    @JsonProperty("PASSWORD")
    @ApiModelProperty(position = 2)
    private String password;
}
