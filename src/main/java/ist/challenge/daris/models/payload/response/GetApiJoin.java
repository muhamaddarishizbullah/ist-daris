package ist.challenge.daris.models.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public interface GetApiJoin {


    @JsonProperty("AUDI_TYPE")
    String getAuditType();

    @JsonProperty("LIMI_MAX_MONTH")
    BigDecimal getLimitMonth();

    @JsonProperty("TYPE")
    String getType();

    @JsonProperty("Tanggal")
    String getCreatedAt();


}
