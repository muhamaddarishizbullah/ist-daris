package ist.challenge.daris.controller;

import ist.challenge.daris.config.JwtUtils;
import ist.challenge.daris.models.entity.ERole;
import ist.challenge.daris.models.entity.Role;
import ist.challenge.daris.models.entity.User;
import ist.challenge.daris.models.payload.request.RegisterRequest;
import ist.challenge.daris.models.payload.request.SignupRequest;
import ist.challenge.daris.models.payload.response.MessageResponse;
import ist.challenge.daris.repository.RoleRepository;
import ist.challenge.daris.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class RegisterController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;
    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest registerRequest) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        if (userRepository.existsByUsername(registerRequest.getUsername())) {
            map.put("HTTP_STATUS_CODE", "409");
            map.put("MESSAGE", "Username sudah terpakai");
            ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            return responseEntity;
        }else{
            // Create new user's account
            User user = new User(registerRequest.getUsername(),
                    encoder.encode(registerRequest.getPassword()));
            userRepository.save(user);
            map.put("HTTP_STATUS_CODE", "201");
            map.put("MESSAGE", "sukses");
            ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            return responseEntity;
        }
    }
}
