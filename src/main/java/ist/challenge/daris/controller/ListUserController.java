package ist.challenge.daris.controller;
import ist.challenge.daris.models.entity.User;
import ist.challenge.daris.models.payload.request.PutUserRequest;
import ist.challenge.daris.models.payload.response.MessageResponse;
import ist.challenge.daris.models.payload.response.UserResponse;
import ist.challenge.daris.repository.UserRepository;
import ist.challenge.daris.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ListUserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    UserServices userServices;

    @GetMapping("/all")
    public ResponseEntity<UserResponse> all() {
        List<UserResponse> userResponse = userServices.getAllUser();
        ResponseEntity responseEntity = new ResponseEntity<>(userResponse, HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping("/put/{id}")
    public ResponseEntity<?> putUser(@PathVariable("id") Long id, @RequestBody @Valid PutUserRequest putUserRequest) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        Optional<User> userData = userRepository.findById(id);
        var userDatas = userData.get();
        if(userRepository.existsByUsername(putUserRequest.getUsername())) {
            map.put("HTTP_STATUS_CODE", "409");
            map.put("MESSAGE", "USERNAME_SUDAH_TERPAKAI");
            map.put("ID", id);
            map.put("USER_NAME", putUserRequest.getUsername());
            ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            return responseEntity;
        }else{
            if(encoder.matches(putUserRequest.getPassword(),userDatas.getPassword())){
                map.put("HTTP_STATUS_CODE", "400");
                map.put("MESSAGE", "PASSWORD_TIDAK_BOLEH_SAMA_DENGAN_PASSWORD_SEBELUMNYA");
                map.put("ID", id);
                map.put("USER_NAME", putUserRequest.getUsername());
                ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
                return responseEntity;
            }else{
                userDatas.setUsername(putUserRequest.getUsername());
                userDatas.setPassword(encoder.encode(putUserRequest.getPassword()));
                userRepository.save(userDatas);
                map.put("HTTP_STATUS_CODE", "201");
                map.put("MESSAGE", "SUKSES");
                map.put("ID", id);
                map.put("USER_NAME", putUserRequest.getUsername());
                ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.OK);
                return responseEntity;
            }
        }
    }
}