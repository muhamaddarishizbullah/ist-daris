package ist.challenge.daris.controller;

import ist.challenge.daris.config.JwtUtils;
import ist.challenge.daris.models.entity.User;
import ist.challenge.daris.models.payload.request.LoginRequest;
import ist.challenge.daris.models.payload.response.JwtResponse;
import ist.challenge.daris.repository.UserRepository;
import ist.challenge.daris.service.impl.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hibernate.criterion.Restrictions.or;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    UserRepository userRepository;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest loginRequest) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        if(loginRequest.getPassword()=="" || loginRequest.getUsername()==""){
            map.put("HTTP_STATUS_CODE", "400");
            map.put("MESSAGE", "Username dan / atau password kosong");
            ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            return responseEntity;
        }else{
            if(userRepository.existsByUsername(loginRequest.getUsername())){
                Optional<User> userData = userRepository.findByUsername(loginRequest.getUsername());
                var userDatas = userData.get();
                if(encoder.matches(loginRequest.getPassword(),userDatas.getPassword())){
                    map.put("HTTP_STATUS_CODE", "200");
                    map.put("MESSAGE", "Sukses Login");
                    ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.OK);
                    return responseEntity;
                }else{
                    map.put("HTTP_STATUS_CODE", "401");
                    map.put("MESSAGE", "Password tidak cocok dengan yang ada di database");
                    ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
                    return responseEntity;
                }
            }else{
                map.put("HTTP_STATUS_CODE", "401");
                map.put("MESSAGE", "Username tidak cocok dengan yang ada di database");
                ResponseEntity responseEntity = new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
                return responseEntity;
            }
        }
    }

}
