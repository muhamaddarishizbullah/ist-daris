package ist.challenge.daris.utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static ist.challenge.daris.utils.Formatter.DATE_FORMAT;

public class Converter {



    public static final Date toDate(String today)
    {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate localDate = LocalDate.parse(today);
       return Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    }

    public static final String dateToString(Date dt) {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.format(dt);
    }

    public static final BigDecimal stringToDecimal(String st) {
        return new BigDecimal(st);
    }


}
