package ist.challenge.daris.utils;

public class EnumStatus {
    //PAYMENT
    public final static int NOT_STARTED=0;
    public final static int PENDING=1;
    public final static int FAIL=2;
    public final static int PAID=3;

    public final static String SUCCESS_CODE="000";
    public final static String SUCCESS_MESSAGE="Success";
    public final static String NOT_FOUND_CODE="001";
    public final static String NOT_FOUND_MESSAGE="Not Found";

    public final static String[] TYPE_CODE          = {"buy","sell","transfer","close"};
    public final static String   TYPE_CODE_BUY       = "buy";
    public final static String   TYPE_CODE_SELL      = "sell";
    public final static String   TYPE_CODE_TRANSFER  = "transfer";
    public final static String   TYPE_CODE_CLOSE     = "close";


    public final static String TYPE_HAS_CREATED="Type Has Created";

    public final static String TYPE_NOT_AVAILABLE_MESSAGE="Limit Not Available";

    public final static String DUPLICATE_CODE="002";
    public final static String DUPLICATE_MESSAGE="duplicate";

    public final static String GENERAL_ERROR_CODE="003";
    public final static String GENERAL_ERROR_MESSAGE="internal server error";

    public final static String BAD_REQUEST_ERROR_CODE="400";
    public final static String BAD_REQUEST_ERROR_MESSAGE="Bad Request";

    public final static String PRECONDITION_FAILED_CODE="412";
    public final static String PRECONDITION_FAILED_MESSAGE ="Precondition Failed";

    //STAGING
    public final static String SUCCESS_STATUS       ="success";
    public final static String NOTSTARTED_STATUS    ="not_started";

    public final static String CURRENCY_USD         ="USD";
    public final static String CURRENCY_IDR         ="IDR";

    public final static String[] CURRENCIES         = {"usd","sgd","gbp","eur","aud","cny","hkd","jpy"};

    public final static String POSITION_LONG = "long";
    public final static String POSITION_SHORT = "short";
    public final static String POSITION_SQUARE = "square";

}
