package ist.challenge.daris.utils;

public class Formatter {
    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
    public final static String DATE_FORMAT="yyyy-MM-dd";
}
