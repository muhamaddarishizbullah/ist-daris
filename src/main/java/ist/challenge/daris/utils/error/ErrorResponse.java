package ist.challenge.daris.utils.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ErrorResponse {
    @JsonProperty("ERROR_CODE")
    private String errorCode;

    @JsonProperty("ERROR_MESSAGE")
    private String errorMsg;

    @JsonProperty("ERRORS")
    private List<ErrorInfo> errorInfos;

}
