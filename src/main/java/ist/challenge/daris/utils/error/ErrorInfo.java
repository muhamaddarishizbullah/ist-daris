package ist.challenge.daris.utils.error;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorInfo {

        private String field;
        private String error;
}
