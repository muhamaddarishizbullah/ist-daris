package ist.challenge.daris.utils.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

import static ist.challenge.daris.utils.EnumStatus.BAD_REQUEST_ERROR_CODE;
import static ist.challenge.daris.utils.EnumStatus.BAD_REQUEST_ERROR_MESSAGE;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        List<ErrorInfo> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(e->new ErrorInfo(e.getField(),e.getDefaultMessage()))
                .collect(Collectors.toList());

        ErrorResponse response=ErrorResponse.builder()
                .errorCode(BAD_REQUEST_ERROR_CODE)
                .errorMsg(BAD_REQUEST_ERROR_MESSAGE)
                .errorInfos(errors)
                .build();

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
