package ist.challenge.daris.webcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class DefaultController {

    @RequestMapping("/")
    public RedirectView  index(Model data, @RequestParam(required=false) String code) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/daris/swagger-ui.html");
        return redirectView;
    }
}
